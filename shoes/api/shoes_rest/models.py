from django.db import models

class BinVO(models.Model):
    import_href = models.CharField(max_length=150, unique=True)
    closet_name = models.CharField(max_length=150)
    # bin_number = models.PositiveSmallIntegerField(null=True)
    # bin_size = models.PositiveSmallIntegerField(null=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length = 150)
    picture_url = models.URLField(null=True)

    bin =  models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model_name



# class BinEncoder(ModelEncoder):
#     model = Bin
#     properties = [
#         "id",
#         "closet_name",
#         "bin_number",
#         "bin_size",
#     ]
