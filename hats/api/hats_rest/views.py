from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric_type",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hat = Hats.objects.filter(location=location_vo_id)
        else:
            hat = Hats.objects.all()
        return JsonResponse(
                {"hat": hat},
                encoder=HatsListEncoder,
            )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_hat_details(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Hats.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Hat does not exist"})
    else:
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )
