# Generated by Django 4.0.3 on 2023-12-14 22:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0006_rename_closet_name_hats_location'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hats',
            old_name='location',
            new_name='closet_name',
        ),
    ]
