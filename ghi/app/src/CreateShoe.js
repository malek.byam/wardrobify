import React, {useState, useEffect} from 'react';

function ShoeForm() {
  const [bins, setBins] = useState([])
  const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const binUrl = 'http://localhost:8080/api/shoes/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(binUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,

      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
              <label htmlFor="starts">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="ends">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="max_presentations">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                <option value="">Select Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;



// manufacturer = models.CharField(max_length=150)
// model_name = models.CharField(max_length=150)
// color = models.CharField(max_length = 150)
// picture_url = models.URLField(null=True)
