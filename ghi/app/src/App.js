import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatsList';
import CreateHatForm from './CreateHat';
import ShoesList from './ShoeList';
import ShoeForm from './CreateShoe';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList />} />
            <Route path="create" element={<CreateHatForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList />} />
            <Route path="create" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
